<?php

namespace Crush;

/**
 * Email
 *
 * 
 *
 * @package     Crush
 * @category	Utilities
 * @author Bruno Foggia
 * @link	https://bitbucket.org/brunnofoggia/crush
 */
class Email {

    /**
     * 
     * @param array $data [*email, name, *subject, *body, *address_to[], address_cc[], address_bcc[], att[]]
     * @param array $sets [*host, *user, *pass, *port, validation, charset, mailtype]
     * @return array [status, error]
     */
    public static function sendSMTP($data, $sets) {
        date_default_timezone_set('America/Sao_Paulo');
        
        $data = static::_SMTPFormatData($data);
        $sets = static::_SMTPFormatSets($sets);
        $valid = static::_SMTPValid($data, $sets);
        
        if($valid['status']!==true) {
            return $valid;
        }

        $mail = new \PHPMailer\PHPMailer\PHPMailer(true);                              // Passing `true` enables exceptions

        try {
            //Server settings
            !empty($sets['charset']) && ($mail->CharSet = $sets['charset']);
//            $mail->SMTPDebug = 2;                                 // Enable verbose debug output
//            $mail->Debugoutput = 'html';
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = $sets['host'];  // Specify main and backup SMTP servers
            $mail->SMTPAuth = (bool) @$sets['validation'];                               // Enable SMTP authentication
            $mail->SMTPAutoTLS = (bool) @$sets['SMTPAutoTLS'];                               // Enable SMTP authentication
            $mail->Username = $sets['user'];                 // SMTP username
            $mail->Password = $sets['pass'];                           // SMTP password
//            $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
            !empty($sets['port']) && ($mail->Port = (int) $sets['port']);                                    // TCP port to connect to
            
            //Recipients
            $mail->setFrom($sets['user']);
            !empty($data['email']) && $mail->addReplyTo($data['email'], !empty($data['name']) ? $data['name'] : $data['email']);
            foreach ((array) @$sets['address_to'] as $email_to) if (!empty($email_to))
                $mail->addAddress($email_to);     // Add a recipient
            foreach ((array) @$sets['address_cc'] as $email_cc) if (!empty($email_cc))
                $mail->addCC($email_cc);
            foreach ((array) @$sets['address_bcc'] as $email_bcc) if (!empty($email_bcc))
                $mail->addBCC($email_bcc);

            //Attachments
            foreach ((array) @$data['att'] as $fileName => $filePath) {
                if (!is_int($fileName)) {
                    $mail->addAttachment($filePath, $fileName);
                } else {
                    $mail->addAttachment($filePath);
                }
            }

            //Content
            $sets['mailtype'] === 'html' && $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = ($data['subject']);
            $mail->Body = ($data['body']);
            !empty($data['alt_body']) && ($mail->AltBody = $data['alt_body']);

            if($mail->send()) {
                return ['status' => true];
            } else {
                return ['status' => false, 'error' => $mail->ErrorInfo];
            }
        } catch (\PHPMailer\PHPMailer\Exception $e) {
            return ['status' => false, 'error' => $mail->ErrorInfo];
        }

        die;
    }
    
    /**
     * Format SMTP email data
     * @param array $data
     * @return array
     */
    protected static function _SMTPFormatData($data) {
        return $data;
    }
    
    /**
     * Format SMTP email sets
     * @param array $sets
     * @return array
     */
    protected static function _SMTPFormatSets($sets) {
        !empty($sets['address_to']) && !is_array($sets['address_to']) && ($sets['address_to'] = [$sets['address_to']]);
        !empty($sets['address_cc']) && !is_array($sets['address_cc']) && ($sets['address_cc'] = [$sets['address_cc']]);
        !empty($sets['address_bcc']) && !is_array($sets['address_bcc']) && ($sets['address_bcc'] = [$sets['address_bcc']]);
        
        return $sets;
    }
    
    /**
     * Checks if all the required data/sets values are present
     * @param array $list
     * @return mixed
     */
    protected static function _SMTPValid($data, $sets) {
        $dataKeyList = static::_SMTPValidData($data);
        $setsKeyList = static::_SMTPValidSets($sets);
        
        if(!empty($dataKeyList) || !empty($setsKeyList)) {
            return ['status'=>false,'missingData'=>$dataKeyList, 'missingSets'=>$setsKeyList];
        }
        return ['status'=>true];
    }
    
    /**
     * Checks if all the required parameters are present
     * @param array $list
     * @return array
     */
    protected static function _SMTPValidSets($list) {
        $keyList = ['host', 'user', 'pass'];
        $invalidKeyList = [];
        foreach($list as $key => $val) {
            if(empty($val) && in_array($key, $keyList)) {
                $invalidKeyList[] = $key;
            }
        }
        return $invalidKeyList;
    }
    
    /**
     * Checks if all the required parameters are present
     * @param array $list
     * @return array
     */
    protected static function _SMTPValidData($list) {
        $keyList = ['email', 'subject', 'body', 'address_to'];
        $invalidKeyList = [];
        foreach($list as $key => $val) {
            if(empty($val) && in_array($key, $keyList)) {
                $invalidKeyList[] = $key;
            }
        }
        return $invalidKeyList;
    }

}
